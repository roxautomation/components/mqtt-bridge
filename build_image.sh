#!/bin/bash
set -e

IMG="roxauto/rox-bridge"
TAG="latest"


# Ensure the script stops on errors and undefined variables
set -o errexit
set -o nounset

# Build image locally and upload it to GitLab
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes

# Check if the builder already exists
if docker buildx inspect mybuilder &>/dev/null; then
    echo "Builder 'mybuilder' already exists. Removing it..."
    docker buildx rm mybuilder
fi

# Create and use a new builder instance
docker buildx create --name mybuilder --use
docker buildx inspect --bootstrap

# Build the image
docker buildx build --platform linux/amd64,linux/arm64,linux/arm/v7 \
       -t "$IMG:$TAG" \
        --push \
        ./docker
