#!/usr/bin/env python3
"""
Test sending messages through the bridge.

Copyright (c) 2024 ROX Automation - Jev Kuznetsov
"""

import asyncio
import logging

import mqtt_client
import ws_client
from roxbot.adapters import MqttLogger

from rox_bridge import bridge_node
from rox_bridge.utils import run_main_async


async def main() -> None:
    node = mqtt_client.MqttClient()
    log_forwarder = MqttLogger(logging.getLogger("node"))  # forward logs to mqtt
    logging.info("Starting web UI")

    async with asyncio.TaskGroup() as tg:
        tg.create_task(bridge_node.main())
        await asyncio.sleep(0.5)
        tg.create_task(log_forwarder.main())
        tg.create_task(node.main())
        tg.create_task(ws_client.main())

        await asyncio.sleep(4)
        raise asyncio.CancelledError("done")


if __name__ == "__main__":
    run_main_async(main())
