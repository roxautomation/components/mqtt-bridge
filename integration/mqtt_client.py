#!/usr/bin/env python3
"""
MQTT client for bridge testing.
Uses `Node` class from `roxbot` package.

Copyright (c) 2024 ROX Automation - Jev Kuznetsov
"""

import asyncio
from roxbot import Node


class MqttClient(Node):
    def __init__(self) -> None:
        super().__init__(name="node")

        # add coroutines to run in main()
        self._coros.append(self._on_init)
        self._coros.append(self.sender)

    async def _on_init(self) -> None:
        """init coroutine to run in main()"""
        self._log.info("Running init coroutine")
        await self.mqtt.register_callback("/to_mqtt", self.receive_cbk)

    def receive_cbk(self, args: list | dict) -> None:
        """example callback function"""
        self._log.info(f"Running callback with {args=}")

    async def sender(self) -> None:
        """example coroutine"""

        counter = 0

        while True:
            # some logging
            # self._log.debug(f"debug message {counter=}")
            self._log.info(f"info message {counter=}")
            counter += 1

            # test message
            msg = {
                "int_val": counter,
                "str_val": f"hello from MQTT {counter}",
                "float_val": 3.14,
            }
            await self.mqtt.publish("/to_ws", msg)

            await asyncio.sleep(1)
