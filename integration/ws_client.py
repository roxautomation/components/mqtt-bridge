#!/usr/bin/env python3
"""
WS client for testing.

Copyright (c) 2024 ROX Automation - Jev Kuznetsov
"""

import asyncio
import websockets
import orjson
import logging
from rox_bridge.ws_leg import WsConfig
from rox_bridge.utils import run_main_async


log = logging.getLogger("ws_client")

CFG = WsConfig()
URI = f"ws://localhost:{CFG.port}"


async def send(websocket: websockets.WebSocketClientProtocol, message: dict) -> None:
    out = orjson.dumps(message)
    log.info(f"Sending {out!r}")
    await websocket.send(out)


async def sender(websocket: websockets.WebSocketClientProtocol) -> None:
    await send(websocket, {"op": "subscribe", "topic": "/to_ws"})
    await send(websocket, {"op": "subscribe", "topic": "/log"})
    await send(websocket, {"topic": "/test", "msg": "hello from client"})

    counter = 0
    while True:
        # test message
        msg = {
            "int_val": counter,
            "str_val": f"hello from WS {counter}",
            "float_val": 42.1,
        }

        message = {
            "op": "publish",
            "topic": "/to_mqtt",
            "msg": msg,
        }
        await send(websocket, message)

        message["msg"] = "simple string"
        await send(websocket, message)

        counter += 1
        await asyncio.sleep(1)


async def receiver(websocket: websockets.WebSocketClientProtocol) -> None:
    async for message in websocket:
        log.info(f"Received {message!r}")


async def main() -> None:
    print(f"Connecting to {URI}")

    log.info("Connecting")
    async with websockets.connect(URI) as websocket:
        log.info("Connected")
        await asyncio.gather(sender(websocket), receiver(websocket))


if __name__ == "__main__":
    run_main_async(main())
