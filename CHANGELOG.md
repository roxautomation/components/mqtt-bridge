# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased


## 1.1.0

* pass messages without parsing


## 1.0.0

* fix ws interface to use "msg" instead of "payload"


## 0.1.2
    * initial release
