#!/bin/bash

# Build the Docker image

IMG_NAME="roxauto/rox-bridge"

docker build -t $IMG_NAME .

docker run -it --rm --network=host $IMG_NAME
